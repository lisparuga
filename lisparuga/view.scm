;;; Lisparuga
;;; Copyright © 2016 David Thompson <davet@gnu.org>
;;;
;;; Lisparuga is free software: you can redistribute it and/or modify
;;; it under the terms of the GNU General Public License as published
;;; by the Free Software Foundation, either version 3 of the License,
;;; or (at your option) any later version.
;;;
;;; Lisparuga is distributed in the hope that it will be useful, but
;;; WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;;; General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with Lisparuga.  If not, see <http://www.gnu.org/licenses/>.

(define-module (lisparuga view)
  #:use-module (ice-9 match)
  #:use-module (sly actor)
  #:use-module (sly live-reload)
  #:use-module (sly math rect)
  #:use-module (sly math vector)
  #:use-module (sly render)
  #:use-module (sly render camera)
  #:use-module (sly render color)
  #:use-module (sly render font)
  #:use-module (sly render sprite)
  #:use-module (sly render sprite-batch)
  #:use-module (sly render tileset)
  #:use-module (sly signal)
  #:use-module (sly utils)
  #:use-module (lisparuga bullets)
  #:use-module (lisparuga config)
  #:use-module (lisparuga enemies)
  #:use-module (lisparuga explosions)
  #:use-module (lisparuga player)
  #:use-module (lisparuga stats)
  #:use-module (lisparuga world)
  #:use-module (lisparuga utils)
  #:export (resolution-scale
            scaled-resolution
            camera
            scaled-camera
            font-color
            load-font*
            render-status-text
            load-sprite/live
            load-tileset/live
            make-chain-sprite
            make-scrolling-background
            render-sprite-maybe
            render-bullets
            render-enemies
            render-player
            render-explosions))

(define resolution-scale 4)
(define scaled-resolution (v* resolution resolution-scale))

(define camera
  (2d-camera #:area (make-rect 0 0 (vx resolution) (vy resolution))))

(define scaled-camera
  (2d-camera #:area (make-rect 0 0
                               (vx scaled-resolution)
                               (vy scaled-resolution))))

(define font-color (rgb #xdeeed6))

(define (load-font* file size)
  (load-font (string-append %datadir "/fonts/" file) size))

(define render-status-text
  (memoize
   (lambda (font text)
     (render-sprite
      (make-label font text #:blended? #f #:anchor 'center)))))

(define (load-sprite* file . keyword-args)
  (apply load-sprite
         (string-append %datadir "/images/" file)
         keyword-args))

(define (load-tileset* file width height)
  (load-tileset (string-append %datadir "/images/" file)
                width height))

(define load-sprite/live (with-live-reload load-sprite*))
(define load-tileset/live (with-live-reload load-tileset*))

(define make-chain-sprite
  (memoize
   (lambda (tileset stats)
     (make-sprite (tileset-ref tileset
                               (- (match (stats-chain-type stats)
                                    ('light 2)
                                    ('dark  5))
                                  (1- (stats-chain-progress stats))))
                  #:anchor 'top-left))))

(define (make-scrolling-background background timer speed)
  (signal-let ((background background)
               (time timer))
    (if background
        (let* ((height (vy resolution))
               (y (- (* (modulo time (round (/ height speed))) speed)))
               (render (render-sprite background)))
          (render-begin
           (move (vector2 0 y) render)
           (move (vector2 0 (+ y height)) render)))
        render-nothing)))

(define (render-sprite-maybe sprite)
  (signal-map (lambda (sprite)
                (if sprite
                    (render-sprite sprite)
                    render-nothing))
              sprite))

(define bullet-rect (make-rect -8 -8 16 16))
(define enemy-rect (make-rect -8 -8 16 16))
(define explosion-rect (make-rect -8 -8 16 16))

(define (render-bullets bullets tileset batch)
  (lambda (context)
    (with-sprite-batch batch context
      (for-each (lambda (actor)
                  (let* ((bullet (actor-ref actor))
                         (rect (rect-move bullet-rect
                                          (bullet-position bullet)))
                         (tex (tileset-ref tileset
                                           (match (bullet-type bullet)
                                             ('player-light 12)
                                             ('player-dark 13)
                                             ('large-light 9)
                                             ('large-dark 8)
                                             ('small-light 11)
                                             ('small-dark 10)))))
                    (sprite-batch-add! batch context tex rect)))
                bullets))))

(define (render-enemies enemies tileset batch time)
  (lambda (context)
    (with-sprite-batch batch context
      (for-each (lambda (actor)
                  (let* ((enemy (actor-ref actor))
                         (hit-time (enemy-last-hit-time enemy))
                         (hit? (and hit-time (zero? (- time hit-time))))
                         (rect (rect-move enemy-rect (enemy-position enemy)))
                         (tex (tileset-ref tileset
                                           (+ (match (enemy-type enemy)
                                                ('popcorn-dark 0)
                                                ('popcorn-light 4)
                                                ('pincer-dark 8)
                                                ('pincer-light 12))
                                              (if hit?
                                                  ;; Use the hit
                                                  ;; indicator frame.
                                                  2
                                                  ;; Swap between the
                                                  ;; 2 normal
                                                  ;; animation frames.
                                                  (modulo (round (/ time 5))
                                                          2))))))
                    (sprite-batch-add! batch context tex rect)))
                enemies))))

(define (render-player player sprite time)
  (if (and (player-invincible? player time)
           (odd? (round (/ time 3))))
      render-nothing
      (move (player-position player)
            (render-sprite sprite))))

(define (render-explosions explosions tileset batch time)
  (lambda (context)
    (with-sprite-batch batch context
      (for-each (lambda (explosion)
                  (let* ((start-time (explosion-time explosion))
                         (rect (rect-move explosion-rect
                                          (explosion-position explosion)))
                         ;; 3 frames of animation.
                         (frame (min 2 (floor (/ (- time start-time) 5))))
                         (tex (tileset-ref tileset frame)))
                    (sprite-batch-add! batch context tex rect)))
                explosions))))
