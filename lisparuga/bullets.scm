;;; Lisparuga
;;; Copyright © 2016 David Thompson <davet@gnu.org>
;;;
;;; Lisparuga is free software: you can redistribute it and/or modify
;;; it under the terms of the GNU General Public License as published
;;; by the Free Software Foundation, either version 3 of the License,
;;; or (at your option) any later version.
;;;
;;; Lisparuga is distributed in the hope that it will be useful, but
;;; WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;;; General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with Lisparuga.  If not, see <http://www.gnu.org/licenses/>.

(define-module (lisparuga bullets)
  #:use-module (sly records)
  #:use-module (sly math rect)
  #:use-module (sly math vector)
  #:use-module (lisparuga utils)
  #:export (%bullet-bounds
            make-bullet
            bullet?
            bullet-type
            bullet-polarity
            bullet-live?
            bullet-position
            bullet-direction
            bullet-hitbox
            kill-bullet
            move-bullet
            move-bullet-to
            bullet-in-bounds?
            forward
            bullet-world-hitbox))

(define %bullet-bounds (rect-inflate bounds 32 32))

(define-record-type* <bullet>
  %make-bullet make-bullet
  bullet?
  (type bullet-type 'generic)
  (polarity bullet-polarity 'light)
  (live? bullet-live? #t)
  (position bullet-position origin2)
  (direction bullet-direction 0)
  (hitbox bullet-hitbox (make-rect -1 -1 1 1)))

(define (kill-bullet bullet)
  (make-bullet #:inherit bullet #:live? #f))

(define (move-bullet bullet offset)
  (make-bullet #:inherit bullet
               #:position (v+ (bullet-position bullet) offset)))

(define (move-bullet-to bullet position)
  (make-bullet #:inherit bullet #:position position))

(define (bullet-in-bounds? bullet)
  (rect-contains? %bullet-bounds (bullet-position bullet)))

(define (forward speed)
  (lambda (world effects bullet)
    (values #f
            effects
            (move-bullet bullet
                         (polar2 speed (bullet-direction bullet))))))

(define (bullet-world-hitbox bullet)
  (rect-move (bullet-hitbox bullet) (bullet-position bullet)))
