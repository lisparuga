;;; Lisparuga
;;; Copyright © 2016 David Thompson <davet@gnu.org>
;;;
;;; Lisparuga is free software: you can redistribute it and/or modify
;;; it under the terms of the GNU General Public License as published
;;; by the Free Software Foundation, either version 3 of the License,
;;; or (at your option) any later version.
;;;
;;; Lisparuga is distributed in the hope that it will be useful, but
;;; WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;;; General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with Lisparuga.  If not, see <http://www.gnu.org/licenses/>.

(define-module (lisparuga game)
  #:use-module (ice-9 format)
  #:use-module (ice-9 match)
  #:use-module (srfi srfi-1)
  #:use-module (sly)
  #:use-module (sly actor)
  #:use-module (sly audio)
  #:use-module (sly fps)
  #:use-module (sly render framebuffer)
  #:use-module (sly render sprite-batch)
  #:use-module (sly render tileset)
  #:use-module (lisparuga audio)
  #:use-module (lisparuga bullets)
  #:use-module (lisparuga enemies)
  #:use-module (lisparuga explosions)
  #:use-module (lisparuga player)
  #:use-module (lisparuga stats)
  #:use-module (lisparuga utils)
  #:use-module (lisparuga view)
  #:use-module (lisparuga world)
  #:export (music
            sound-effects
            scene))


;;;
;;; Controller
;;;

(define-signal timer (signal-timer))

(define-signal world
  (signal-fold world-eval
               %default-world
               (signal-merge
                (make-signal '(null))
                (signal-let ((time timer))
                  `(tick ,time))
                (signal-let ((direction key-arrows))
                  `(player-direction ,direction))
                (signal-let ((shoot? (signal-drop-repeats (key-down? 'z))))
                  `(player-shoot ,shoot?))
                (signal-let ((toggle?
                              (signal-filter identity #f
                                             (signal-drop-repeats
                                              (key-down? 'x)))))
                  `(player-toggle-polarity))
                (signal-let ((restart? (key-down? 'return)))
                  `(restart ,restart?)))))

(define-signal display-fps? (key-toggle 'f))


;;;
;;; Music and Sound
;;;

(define-signal music
  (signal-drop-repeats
   (signal-let ((world world))
     (load-music* (cond
                   ((game-intro? world) "title-screen.ogg")
                   ((game-over? world) "ending.ogg")
                   ((game-won? world) "title-screen.ogg")
                   (else "level-2.ogg"))))))


(define-signal sounds
  (on-start
   `((enemy-hit . ,(load-sample* "hit.wav"))
     (explosion . ,(load-sample* "explosion.wav"))
     (player-death . ,(load-sample* "player-death.wav"))
     (player-shoot . ,(load-sample* "player-shoot.wav")))))

(define-signal sound-effects
  (signal-let ((world world)
               (time timer)
               (sounds sounds))
    (filter-map (lambda (sound-proc)
                  (let ((sound (sound-proc world time)))
                    (and sound (assq-ref sounds sound))))
                (list enemy-hit-sound
                      player-shoot-sound
                      player-death-sound
                      explosion-sound))))


;;;
;;; View
;;;

(define-signal framebuffer
  (on-start
   (make-framebuffer (vx scaled-resolution) (vy scaled-resolution))))

(define-signal framebuffer-sprite
  (signal-map-maybe (lambda (framebuffer)
                      (make-sprite (framebuffer-texture framebuffer)
                                   #:anchor 'bottom-left))
                    framebuffer))

(define-signal font
  (on-start
   (load-font* "kenpixel_mini.ttf" 7)))

(define-signal big-font
  (on-start
   (load-font* "kenpixel_mini.ttf" 16)))

(define-signal fps-text
  (signal-let ((fps fps)
               (font font))
    (if font
        (move (vector2 (vx resolution) 0)
              (render-sprite
               (make-label font
                           (format #f "~d fps" fps)
                           #:blended? #f
                           #:anchor 'bottom-right)))
        render-nothing)))

(define-signal score-text
  (signal-let ((font font)
               (world world))
    (if font
        (move resolution
              (render-sprite
               (make-label font
                           (number->string (stats-score (world-stats world)))
                           #:blended? #f
                           #:anchor 'top-right)))
        render-nothing)))

(define-signal lives-text
  (signal-let ((font font)
               (world world))
    (if font
        (move (vector2 (/ (vx resolution) 2) (vy resolution))
              (render-sprite
               (make-label font
                           (format #f "~d ship"
                                   (stats-lives (world-stats world)))
                           #:blended? #f
                           #:anchor 'top-center)))
        render-nothing)))

(define-signal chain-text
  (signal-let ((font font)
               (world world))
    (if font
        (move (vector2 1 (vy resolution))
              (render-sprite
               (make-label font
                           (format #f "~d chain"
                                   (stats-chain (world-stats world)))
                           #:blended? #f
                           #:anchor 'top-left)))
        render-nothing)))

(define-signal status-text
  (signal-let ((big-font big-font)
               (font font)
               (world world))
    (cond
     ((or (not font) (not big-font))
      render-nothing)
     ((game-over? world)
      (move (v* resolution 0.5)
            (render-begin
             (render-status-text big-font "GAME OVER")
             (move (vector2 0 -16)
                   (render-status-text font "Press ENTER to play again"))
             (move (vector2 0 -28)
                   (render-status-text font "Press ESC to quit")))))
     ((game-won? world)
      (move (v* resolution 0.5)
            (render-begin
             (render-status-text big-font "COMPLETE!")
             (move (vector2 0 -16)
                   (render-status-text font "Press ENTER to play again"))
             (move (vector2 0 -28)
                   (render-status-text font "Press ESC to quit")))))
     ((game-intro? world)
      (move (v* resolution (vector2 0.5 0.8))
            (render-begin
             (render-status-text font "Use arrow keys to move")
             (move (vector2 0 -12)
                   (render-status-text font "Press Z to shoot"))
             (move (vector2 0 -24)
                   (render-status-text font "Press X to change polarity"))
             (move (vector2 0 -36)
                   (render-status-text font "Press ESC to quit"))
             (move (vector2 0 -60)
                   (render-status-text big-font "Press ENTER")))))
     (else
      render-nothing))))

(define-signal background
  (load-sprite/live "background.png"
                    #:anchor 'bottom-left))

(define-signal background-overlay
  (load-sprite/live "background-overlay.png"
                    #:anchor 'bottom-left))

(define-signal player-tileset
  (load-tileset/live "player.png" 16 16))

(define-signal bullet-tileset
  (load-tileset/live "bullets.png" 16 16))

(define-signal enemy-tileset
  (load-tileset/live "enemies.png" 16 16))

(define-signal explosion-tileset
  (load-tileset/live "explosion.png" 16 16))

(define-signal chain-tileset
  (load-tileset/live "chain.png" 24 16))

(define-signal chain-sprite
  (signal-map-maybe (lambda (world tileset)
                      (let ((stats (world-stats world)))
                        (if (zero? (stats-chain-progress stats))
                            render-nothing
                            (move (vector2 0 (- (vy resolution) 5))
                                  (render-sprite
                                   (make-chain-sprite tileset stats))))))
                    world
                    chain-tileset))

(define-signal player-sprite
  (signal-map-maybe (lambda (world tileset)
                      (make-sprite
                       (let* ((player (actor-ref (world-player world)))
                              (dx (vx (player-direction player)))
                              (offset (cond
                                       ((zero? dx) 0)
                                       ((positive? dx) 1)
                                       ((negative? dx) 2))))
                         (tileset-ref tileset
                                      (+ (match (player-polarity player)
                                           ('light 12)
                                           ('dark 8))
                                         offset)))))
                    world
                    player-tileset))

(define-signal scrolling-background
  (signal-map render-begin
              ;;(make-scrolling-background background timer 0.2)
              (render-sprite-maybe background)
              (make-scrolling-background background-overlay timer 4)))

(define-signal batch
  (on-start (make-sprite-batch 1000)))

(define-signal scene
  (signal-let ((fps-text fps-text)
               (score-text score-text)
               (lives-text lives-text)
               (chain-text chain-text)
               (status-text status-text)
               (display-fps? display-fps?)
               (background scrolling-background)
               (framebuffer framebuffer)
               (framebuffer-sprite framebuffer-sprite)
               (player-sprite player-sprite)
               (chain-sprite chain-sprite)
               (bullet-tileset bullet-tileset)
               (enemy-tileset enemy-tileset)
               (explosion-tileset explosion-tileset)
               (batch batch)
               (world world)
               (time timer))
    (if (and framebuffer framebuffer-sprite batch bullet-tileset
             enemy-tileset player-sprite explosion-tileset chain-sprite)
        (let ((player (actor-ref (world-player world))))
          (render-begin
           (with-framebuffer framebuffer
             (with-camera camera
               (render-begin
                background
                (render-explosions (world-explosions world)
                                   explosion-tileset
                                   batch
                                   time)
                (render-bullets (world-player-bullets world)
                                bullet-tileset
                                batch)
                (if (game-over? world)
                    render-nothing
                    (render-player player player-sprite time))
                (render-enemies (world-enemies world)
                                enemy-tileset
                                batch
                                time)
                (render-bullets (world-enemy-bullets world)
                                bullet-tileset
                                batch)
                (with-color font-color
                  (render-begin
                   (if display-fps?
                       fps-text
                       render-nothing)
                   score-text
                   lives-text
                   chain-text
                   status-text))
                chain-sprite)))
           (with-camera scaled-camera
             (scale resolution-scale (render-sprite framebuffer-sprite)))))
        render-nothing)))
