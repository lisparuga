;;; Lisparuga
;;; Copyright © 2016 David Thompson <davet@gnu.org>
;;;
;;; Lisparuga is free software: you can redistribute it and/or modify
;;; it under the terms of the GNU General Public License as published
;;; by the Free Software Foundation, either version 3 of the License,
;;; or (at your option) any later version.
;;;
;;; Lisparuga is distributed in the hope that it will be useful, but
;;; WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;;; General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with Lisparuga.  If not, see <http://www.gnu.org/licenses/>.

(define-module (lisparuga explosions)
  #:use-module (sly records)
  #:use-module (sly math vector)
  #:use-module (lisparuga utils)
  #:export (make-explosion
            explosion?
            explosion-type
            explosion-position
            explosion-time
            explosion-active?))

(define-record-type* <explosion>
  %make-explosion make-explosion
  explosion?
  (type explosion-type 'regular)
  (position explosion-position origin2)
  (time explosion-time 0))

(define (explosion-active? explosion current-time)
  (< (- current-time (explosion-time explosion)) 15))
