;;; Lisparuga
;;; Copyright © 2016 David Thompson <davet@gnu.org>
;;;
;;; Lisparuga is free software: you can redistribute it and/or modify
;;; it under the terms of the GNU General Public License as published
;;; by the Free Software Foundation, either version 3 of the License,
;;; or (at your option) any later version.
;;;
;;; Lisparuga is distributed in the hope that it will be useful, but
;;; WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;;; General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with Lisparuga.  If not, see <http://www.gnu.org/licenses/>.

(define-module (lisparuga audio)
  #:use-module (srfi srfi-1)
  #:use-module (sly actor)
  #:use-module (sly audio)
  #:use-module (sly utils)
  #:use-module (lisparuga config)
  #:use-module (lisparuga enemies)
  #:use-module (lisparuga explosions)
  #:use-module (lisparuga player)
  #:use-module (lisparuga world)
  #:export (load-music*
            load-sample*
            loop-music
            enemy-hit-sound
            player-shoot-sound
            player-death-sound
            explosion-sound
            play-sound-effects))

(define load-music*
  (memoize
   (lambda (file)
     (load-music (string-append %datadir "/music/" file)))))

(define (loop-music music)
  (play-music music #:loop? #t))

(define (load-sample* file)
  (load-sample (string-append %datadir "/sounds/" file)))

(define (enemy-hit-sound world time)
  (and (any (lambda (enemy)
              (let ((hit-time (enemy-last-hit-time (actor-ref enemy))))
                (and hit-time (= time hit-time))))
            (world-enemies world))
       'enemy-hit))

(define (player-shoot-sound world time)
  (and (zero? (modulo time 5))
       (player-shooting? (actor-ref (world-player world)))
       'player-shoot))

(define (player-death-sound world time)
  (and (let ((death-time (player-last-death-time
                          (actor-ref (world-player world)))))
         (and death-time (= time death-time)))
       'player-death))

(define (explosion-sound world time)
  (and (any (lambda (explosion)
              (let ((explode-time (explosion-time explosion)))
                (= time explode-time)))
            (world-explosions world))
       'explosion))

(define (play-sound-effects sounds)
  (for-each play-sample sounds))
