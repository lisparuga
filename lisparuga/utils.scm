;;; Lisparuga
;;; Copyright © 2016 David Thompson <davet@gnu.org>
;;;
;;; Lisparuga is free software: you can redistribute it and/or modify
;;; it under the terms of the GNU General Public License as published
;;; by the Free Software Foundation, either version 3 of the License,
;;; or (at your option) any later version.
;;;
;;; Lisparuga is distributed in the hope that it will be useful, but
;;; WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;;; General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with Lisparuga.  If not, see <http://www.gnu.org/licenses/>.

(define-module (lisparuga utils)
  #:use-module (sly input keyboard)
  #:use-module (sly math rect)
  #:use-module (sly math vector)
  #:use-module (sly signal)
  #:export (resolution
            origin2
            bounds
            key-toggle))

(define resolution (vector2 120 160))
(define origin2 (vector2 0 0))
(define bounds (make-rect (vector2 0 0) resolution))

(define (key-toggle key)
  "Create a signal that is initially #f and toggles between #t and #f
each time KEY is pressed."
  (signal-fold (lambda (down? previous)
                 (and down? (not previous)))
               #f
               (signal-filter identity #f
                              ;; Ignore repeated key down signals.
                              (signal-drop-repeats (key-down? key)))))
