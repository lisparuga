;;; Lisparuga
;;; Copyright © 2016 David Thompson <davet@gnu.org>
;;;
;;; Lisparuga is free software: you can redistribute it and/or modify
;;; it under the terms of the GNU General Public License as published
;;; by the Free Software Foundation, either version 3 of the License,
;;; or (at your option) any later version.
;;;
;;; Lisparuga is distributed in the hope that it will be useful, but
;;; WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;;; General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with Lisparuga.  If not, see <http://www.gnu.org/licenses/>.

(define-module (lisparuga stats)
  #:use-module (sly records)
  #:use-module (lisparuga enemies)
  #:use-module (lisparuga player)
  #:use-module (lisparuga utils)
  #:export (make-stats
            stats?
            stats-score
            stats-lives
            stats-chain
            stats-chain-type
            stats-chain-progress
            decrement-life
            add-to-score
            add-to-chain))

(define-record-type* <stats>
  %make-stats make-stats
  stats?
  (score stats-score 0)
  (lives stats-lives 3)
  (chain stats-chain 0)
  (chain-type stats-chain-type #f)
  (chain-progress stats-chain-progress 0))

(define (decrement-life stats)
  (make-stats #:inherit stats
              #:lives (max 0 (1- (stats-lives stats)))))

(define max-chain-multiplier 10)

(define (add-to-score enemy stats)
  ;; TODO: Award different points for different types of enemies.
  (make-stats #:inherit stats
              #:score (+ (stats-score stats)
                         1000          ; base kill points
                         ;; Chain multiplier.
                         (* 255
                            (min (stats-chain stats)
                                 max-chain-multiplier)))))

(define (add-to-chain enemy stats)
  (let* ((enemy-polarity (enemy-polarity enemy))
         (chain-polarity (stats-chain-type stats))
         (progress (stats-chain-progress stats)))
    (cond
     ((or (zero? progress) (= progress 3))
      (make-stats #:inherit stats
                  #:chain-type enemy-polarity
                  #:chain-progress 1))
     ((not (eq? enemy-polarity chain-polarity))
      (make-stats #:inherit stats
                  #:chain-type #f
                  #:chain-progress 0
                  #:chain 0))
     ((= progress 1)
      (make-stats #:inherit stats
                  #:chain-progress 2))
     ((= progress 2)
      (make-stats #:inherit stats
                  #:chain-progress 3
                  #:chain (1+ (stats-chain stats)))))))
