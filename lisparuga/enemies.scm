;;; Lisparuga
;;; Copyright © 2016 David Thompson <davet@gnu.org>
;;;
;;; Lisparuga is free software: you can redistribute it and/or modify
;;; it under the terms of the GNU General Public License as published
;;; by the Free Software Foundation, either version 3 of the License,
;;; or (at your option) any later version.
;;;
;;; Lisparuga is distributed in the hope that it will be useful, but
;;; WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;;; General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with Lisparuga.  If not, see <http://www.gnu.org/licenses/>.

(define-module (lisparuga enemies)
  #:use-module (sly records)
  #:use-module (sly math rect)
  #:use-module (sly math vector)
  #:use-module (lisparuga bullets)
  #:use-module (lisparuga player)
  #:use-module (lisparuga utils)
  #:export (make-enemy
            enemy?
            enemy-position
            enemy-aim
            enemy-type
            enemy-polarity
            enemy-hitbox
            enemy-last-hit-time
            enemy-health
            enemy-dead?
            enemy-alive?
            damage-enemy
            place-enemy
            move-enemy
            aim-enemy
            enemy-world-hitbox))

(define-record-type* <enemy>
  %make-enemy make-enemy
  enemy?
  (position enemy-position origin2)
  (aim enemy-aim 0) ; angle for firing bullets
  ;; TODO: We could leave out the '-light' part and use the polarity
  ;; field to figure things out, but it's more work so forget it.
  (type enemy-type 'pincer-dark)
  (polarity enemy-polarity 'light)
  (hitbox enemy-hitbox (make-rect -5 -5 10 10))
  (last-hit-time enemy-last-hit-time #f)
  (health enemy-health 0))

(define (enemy-dead? enemy)
  (zero? (enemy-health enemy)))

(define (enemy-alive? enemy)
  (> (enemy-health enemy) 0))

(define (damage-enemy enemy bullet time)
  (make-enemy #:inherit enemy
              #:last-hit-time time
              #:health (max 0
                            (- (enemy-health enemy)
                               (if (eq? (enemy-polarity enemy)
                                        (bullet-polarity bullet))
                                   %player-attack
                                   ;; Bullets of opposite polarity
                                   ;; deal double damage.
                                   (* %player-attack 2))))))

(define (place-enemy enemy position)
  (make-enemy #:inherit enemy
              #:position position))

(define (move-enemy enemy offset)
  (make-enemy #:inherit enemy
              #:position (v+ (enemy-position enemy) offset)))

(define (aim-enemy enemy offset)
  (make-enemy #:inherit enemy
              #:aim (+ (enemy-aim enemy) offset)))

(define (enemy-world-hitbox enemy)
  (rect-move (enemy-hitbox enemy) (enemy-position enemy)))
